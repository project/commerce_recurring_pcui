<?php

namespace Drupal\commerce_recurring_pcui\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PlanChangeConfigForm.
 */
class PlanChangeConfigForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig $commerce_recurring_pcui_config */
    $commerce_recurring_pcui_config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $commerce_recurring_pcui_config->label(),
      '#description' => $this->t("Label for the Plan Change Configuration."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $commerce_recurring_pcui_config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig::load',
      ],
      '#disabled' => !$commerce_recurring_pcui_config->isNew(),
    ];

    // TODO: Actually load plugins dynamically.
    // TODO: Show a description of each plugin to help the user select.
    $form['strategy'] = [
      '#type' => 'radios',
      '#options' => [
        'allow_prorating' => $this->t('Allow prorating'),
        'avoid_prorating' => $this->t('Avoid prorating'),
      ],
      '#title' => $this->t('Plan change strategy'),
      '#description' => $this->t('Choose the strategy to be used when determining pricing and behavior for plan changes. ADD PLUGIN-LEVEL DESCRIPTIONS HERE.'),
      '#required' => TRUE,
      '#default_value' => $commerce_recurring_pcui_config->getStrategyPluginId(),
    ];
    // TODO: strategy plugin config?
    // .
    // UPGRADING
    // .
    $form['upgradeButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to start an upgrade.'),
      '#title' => $this->t('Label for upgrade button'),
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeButtonLabel() ?: '',
    ];

    $form['upgradeConfirmText'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the explanation shown to the user after starting an upgrade.'),
      '#title' => $this->t('Text to show when confirming an upgrade'),
      '#rows' => 5,
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeConfirmText(),
    ];

    $form['upgradeConfirmButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to confirm an upgrade.'),
      '#title' => $this->t('Label for upgrade confirmation button'),
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeConfirmButtonLabel(),
    ];

    $form['upgradeCancelButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to cancel an upgrade.'),
      '#title' => $this->t('Label for do-not-upgrade button'),
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeCancelButtonLabel(),
    ];

    $form['upgradeCompleteMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after completing an upgrade.'),
      '#title' => $this->t('Message to show after upgrading'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeCompleteMessage(),
    ];

    $form['upgradeCanceledMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after canceling an upgrade.'),
      '#title' => $this->t('Message to show after deciding not to upgrade'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getUpgradeCanceledMessage(),
    ];

    // .
    // DOWNGRADING
    // .
    $form['downgradeButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to start a downgrade.'),
      '#title' => $this->t('Label for downgrade button'),
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeButtonLabel(),
    ];

    $form['downgradeConfirmText'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the explanation shown to the user after starting a downgrade.'),
      '#title' => $this->t('Text to show when confirming a downgrade'),
      '#rows' => 5,
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeConfirmText(),
    ];

    $form['downgradeConfirmButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to confirm a downgrade.'),
      '#title' => $this->t('Label for downgrade confirmation button'),
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeConfirmButtonLabel(),
    ];

    $form['downgradeCancelButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to cancel a downgrade.'),
      '#title' => $this->t('Label for do-not-downgrade button'),
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeCancelButtonLabel(),
    ];

    $form['downgradeCompleteMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after completing a downgrade.'),
      '#title' => $this->t('Message to show after downgrading'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeCompleteMessage(),
    ];

    $form['downgradeCanceledMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after canceling a downgrade.'),
      '#title' => $this->t('Message to show after deciding not to downgrade'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getDowngradeCanceledMessage(),
    ];

    // .
    // CANCELING
    // .
    $form['cancelButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to start a cancellation.'),
      '#title' => $this->t('Label for cancel button'),
      '#default_value' => $commerce_recurring_pcui_config->getCancelButtonLabel(),
    ];

    $form['cancelConfirmText'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the explanation shown to the user after starting a cancellation.'),
      '#title' => $this->t('Text to show when confirming cancellation'),
      '#rows' => 5,
      '#default_value' => $commerce_recurring_pcui_config->getCancelConfirmText(),
    ];

    $form['cancelConfirmButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to confirm cancellation.'),
      '#title' => $this->t('Label for cancellation confirmation button'),
      '#default_value' => $commerce_recurring_pcui_config->getCancelConfirmButtonLabel(),
    ];

    $form['cancelCancelButtonLabel'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter the wording you want to use for the button the user clicks to cancel cancellation.'),
      '#title' => $this->t('Label for do-not-cancel button'),
      '#default_value' => $commerce_recurring_pcui_config->getCancelCancelButtonLabel(),
    ];

    $form['cancelCompleteMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after completing cancellation.'),
      '#title' => $this->t('Message to show after canceling'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getCancelCompleteMessage(),
    ];

    $form['cancelCanceledMessage'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Enter the message shown to the user after canceling cancellation.'),
      '#title' => $this->t('Message to show after deciding not to cancel'),
      '#rows' => 3,
      '#default_value' => $commerce_recurring_pcui_config->getCancelCanceledMessage(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $commerce_recurring_pcui_config = $this->entity;
    $status = $commerce_recurring_pcui_config->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label plan change configuration.', [
          '%label' => $commerce_recurring_pcui_config->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label plan change configuration.', [
          '%label' => $commerce_recurring_pcui_config->label(),
        ]));
    }
    $form_state->setRedirectUrl($commerce_recurring_pcui_config->toUrl('collection'));
  }

}
