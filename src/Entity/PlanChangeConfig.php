<?php

namespace Drupal\commerce_recurring_pcui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Plan Change Configuration entity.
 *
 * @ConfigEntityType(
 *   id = "commerce_recurring_pcui_config",
 *   label = @Translation("Plan Change Configuration"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\commerce_recurring_pcui\PlanChangeConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_recurring_pcui\Form\PlanChangeConfigForm",
 *       "edit" = "Drupal\commerce_recurring_pcui\Form\PlanChangeConfigForm",
 *       "delete" =
 *   "Drupal\commerce_recurring_pcui\Form\PlanChangeConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\commerce_recurring_pcui\PlanChangeConfigHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "commerce_recurring_pcui_config",
 *   config_export = {
 *     "id",
 *     "label",
 *     "strategy",
 *     "upgradeButtonLabel",
 *     "upgradeConfirmText",
 *     "upgradeConfirmButtonLabel",
 *     "upgradeCancelButtonLabel",
 *     "upgradeCompleteMessage",
 *     "upgradeCanceledMessage",
 *     "downgradeButtonLabel",
 *     "downgradeConfirmText",
 *     "downgradeConfirmButtonLabel",
 *     "downgradeCancelButtonLabel",
 *     "downgradeCompleteMessage",
 *     "downgradeCanceledMessage",
 *     "cancelButtonLabel",
 *     "cancelConfirmText",
 *     "cancelConfirmButtonLabel",
 *     "cancelCancelButtonLabel",
 *     "cancelCompleteMessage",
 *     "cancelCanceledMessage",
 *   },
 *   admin_permission = "administer commerce_recurring_pcui_config",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/commerce/config/plan-change-config/{commerce_recurring_pcui_config}",
 *     "add-form" = "/admin/commerce/config/plan-change-config/add",
 *     "edit-form" =
 *   "/admin/commerce/config/plan-change-config/{commerce_recurring_pcui_config}/edit",
 *     "delete-form" =
 *   "/admin/commerce/config/plan-change-config/{commerce_recurring_pcui_config}/delete",
 *     "collection" = "/admin/commerce/config/plan-change-config"
 *   }
 * )
 */
class PlanChangeConfig extends ConfigEntityBase implements PlanChangeConfigInterface {

  /**
   * The Plan Change Configuration ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Plan Change Configuration label.
   *
   * @var string
   */
  protected $label;

  /**
   * The strategy to use when handling plan changes.
   *
   * @var string
   *
   * @todo: make this a constant
   */
  protected $strategy = '';

  /**
   * The label to show on the upgrade button.
   *
   * @var string
   */
  protected $upgradeButtonLabel = '';

  /**
   * The text for confirming the upgrade.
   *
   * @var string
   */
  protected $upgradeConfirmText = '';

  /**
   * The label on the button that confirms the upgrade.
   *
   * @var string
   */
  protected $upgradeConfirmButtonLabel = '';

  /**
   * The button that stops the upgrade process.
   *
   * @var string
   */
  protected $upgradeCancelButtonLabel = '';

  /**
   * The message shown after a successful upgrade.
   *
   * @var string
   */
  protected $upgradeCompleteMessage = '';

  /**
   * The message shown after the upgrade is stopped.
   *
   * @var string
   */
  protected $upgradeCanceledMessage = '';

  /**
   * The label to show on the downgrade button.
   *
   * @var string
   */
  protected $downgradeButtonLabel = '';

  /**
   * The text for confirming the downgrade.
   *
   * @var string
   */
  protected $downgradeConfirmText = '';

  /**
   * The label on the button that confirms the downgrade.
   *
   * @var string
   */
  protected $downgradeConfirmButtonLabel = '';

  /**
   * The button that stops the downgrade process.
   *
   * @var string
   */
  protected $downgradeCancelButtonLabel = '';

  /**
   * The message shown after a successful downgrade.
   *
   * @var string
   */
  protected $downgradeCompleteMessage = '';

  /**
   * The message shown after the downgrade is stopped.
   *
   * @var string
   */
  protected $downgradeCanceledMessage = '';

  /**
   * The label to show on the cancel button.
   *
   * @var string
   */
  protected $cancelButtonLabel = '';

  /**
   * The text for confirming cancellation.
   *
   * @var string
   */
  protected $cancelConfirmText = '';

  /**
   * The label on the button that confirms cancellation.
   *
   * @var string
   */
  protected $cancelConfirmButtonLabel = '';

  /**
   * The button that stops the cancellation process.
   *
   * @var string
   */
  protected $cancelCancelButtonLabel = '';

  /**
   * The message shown upon successful cancellation.
   *
   * @var string
   */
  protected $cancelCompleteMessage = '';

  /**
   * The message shown after cancellation is stopped.
   *
   * @var string
   */
  protected $cancelCanceledMessage = '';

  /**
   * {@inheritdoc}
   *
   * @todo: return plugin instance
   */
  public function getStrategyPluginId(): string {
    return $this->strategy;
  }

  /**
   * {@inheritdoc}
   *
   * @todo: Flesh out
   */
  public function setStrategyPluginId(string $strategy_plugin_id): void {
    $this->strategy = $strategy_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeButtonLabel(): string {
    return $this->upgradeButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeConfirmText(): string {
    return $this->upgradeConfirmText;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeConfirmButtonLabel(): string {
    return $this->upgradeConfirmButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeCancelButtonLabel(): string {
    return $this->upgradeCancelButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeCompleteMessage(): string {
    return $this->upgradeCompleteMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpgradeCanceledMessage(): string {
    return $this->upgradeCanceledMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeButtonLabel(): string {
    return $this->downgradeButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeConfirmText(): string {
    return $this->downgradeConfirmText;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeConfirmButtonLabel(): string {
    return $this->downgradeConfirmButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeCancelButtonLabel(): string {
    return $this->downgradeCancelButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeCompleteMessage(): string {
    return $this->downgradeCompleteMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getDowngradeCanceledMessage(): string {
    return $this->downgradeCanceledMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelButtonLabel(): string {
    return $this->cancelButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelConfirmText(): string {
    return $this->cancelConfirmText;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelConfirmButtonLabel(): string {
    return $this->cancelConfirmButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelCancelButtonLabel(): string {
    return $this->cancelCancelButtonLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelCompleteMessage(): string {
    return $this->cancelCompleteMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelCanceledMessage(): string {
    return $this->cancelCanceledMessage;
  }

}
