<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

/**
 * Tests the main configuration form for plan change configurations.
 */
class PlanChangeConfigFormTest extends PcuiTestBase {

  /**
   * Test the configuration form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testForm() {
    $this->drupalGet('/admin/commerce/config/plan-change-config/add');

    // Check that all fields are present.
    $this->assertSession()->pageTextContains('Plan change strategy');

    $this->assertSession()->pageTextContains('Label for upgrade button');
    $this->assertSession()->pageTextContains('Text to show when confirming an upgrade');
    $this->assertSession()->pageTextContains('Label for upgrade confirmation button');
    $this->assertSession()->pageTextContains('Label for do-not-upgrade button');
    $this->assertSession()->pageTextContains('Message to show after upgrading');
    $this->assertSession()->pageTextContains('Message to show after deciding not to upgrade');

    $this->assertSession()->pageTextContains('Label for downgrade button');
    $this->assertSession()->pageTextContains('Text to show when confirming a downgrade');
    $this->assertSession()->pageTextContains('Label for downgrade confirmation button');
    $this->assertSession()->pageTextContains('Label for do-not-downgrade button');
    $this->assertSession()->pageTextContains('Message to show after downgrading');
    $this->assertSession()->pageTextContains('Message to show after deciding not to downgrade');

    $this->assertSession()->pageTextContains('Label for cancel button');
    $this->assertSession()->pageTextContains('Text to show when confirming cancellation');
    $this->assertSession()->pageTextContains('Label for cancellation confirmation button');
    $this->assertSession()->pageTextContains('Label for do-not-cancel button');
    $this->assertSession()->pageTextContains('Message to show after canceling');
    $this->assertSession()->pageTextContains('Message to show after deciding not to cancel');

    // Fill out the fields and ensure we can save the form.
    $this->createPlanChangeConfig();

    // Load the plan change configuration we created and check the field values.
    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfigInterface $planChangeConfig */
    $planChangeConfig = $this->container->get('entity_type.manager')
      ->getStorage('commerce_recurring_pcui_config')
      ->load('test_pcui_config');

    self::assertEquals('Test PCUI config', $planChangeConfig->label());
    self::assertEquals('avoid_prorating', $planChangeConfig->getStrategyPluginId());

    self::assertEquals('Upgrade', $planChangeConfig->getUpgradeButtonLabel());
    self::assertEquals('You are about to upgrade from [from-variation:label] to [to-variation:label]. You will be charged [upgrade-order:order-price] immediately. Would you like to finish the upgrade?', $planChangeConfig->getUpgradeConfirmText());
    self::assertEquals('Finish upgrade', $planChangeConfig->getUpgradeConfirmButtonLabel());
    self::assertEquals('Do not upgrade', $planChangeConfig->getUpgradeCancelButtonLabel());
    self::assertEquals('Your plan has been changed.', $planChangeConfig->getUpgradeCompleteMessage());
    self::assertEquals('You stopped the upgrade. Your plan has not been changed.', $planChangeConfig->getUpgradeCanceledMessage());

    self::assertEquals('Downgrade', $planChangeConfig->getDowngradeButtonLabel());
    self::assertEquals('You are about to downgrade from [from-variation:label] to [to-variation:label]. The changes will take effect at the start of the next billing period ([next-billing-period:start]).', $planChangeConfig->getDowngradeConfirmText());
    self::assertEquals('Schedule downgrade', $planChangeConfig->getDowngradeConfirmButtonLabel());
    self::assertEquals('Do not downgrade', $planChangeConfig->getDowngradeCancelButtonLabel());
    self::assertEquals('Your plan is scheduled to be downgraded on [next-billing-period:start].', $planChangeConfig->getDowngradeCompleteMessage());
    self::assertEquals('You stopped the downgrade. Your plan has not been changed.', $planChangeConfig->getDowngradeCanceledMessage());

    self::assertEquals('Cancel', $planChangeConfig->getCancelButtonLabel());
    self::assertEquals('You are about to cancel your [from-variation:label] subscription. Your subscription will expire on [billing-period:end].', $planChangeConfig->getCancelConfirmText());
    self::assertEquals('Schedule cancellation', $planChangeConfig->getCancelConfirmButtonLabel());
    self::assertEquals('Do not cancel', $planChangeConfig->getCancelCancelButtonLabel());
    self::assertEquals("You've canceled your subscription and you will lose access on [billing-period:end].", $planChangeConfig->getCancelCompleteMessage());
    self::assertEquals('You stopped the cancellation. You are still subscribed.', $planChangeConfig->getCancelCanceledMessage());
  }

  /**
   * Test updating an PCUI config via the form.
   *
   * Also ensure that the configuration form renders default values.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testFormUpdate() {
    // Create our default plan change config.
    $this->createPlanChangeConfig();

    // Get the plan edit page.
    $this->drupalGet('/admin/commerce/config/plan-change-config/test_pcui_config/edit');

    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfigInterface $planChangeConfig */
    $planChangeConfig = $this->container->get('entity_type.manager')
      ->getStorage('commerce_recurring_pcui_config')
      ->load('test_pcui_config');

    // Assert that all fields are filled with default values.
    self::assertEquals('Test PCUI config', $planChangeConfig->label());
    self::assertEquals('avoid_prorating', $planChangeConfig->getStrategyPluginId());

    self::assertEquals('Upgrade', $planChangeConfig->getUpgradeButtonLabel());
    self::assertEquals('You are about to upgrade from [from-variation:label] to [to-variation:label]. You will be charged [upgrade-order:order-price] immediately. Would you like to finish the upgrade?', $planChangeConfig->getUpgradeConfirmText());
    self::assertEquals('Finish upgrade', $planChangeConfig->getUpgradeConfirmButtonLabel());
    self::assertEquals('Do not upgrade', $planChangeConfig->getUpgradeCancelButtonLabel());
    self::assertEquals('Your plan has been changed.', $planChangeConfig->getUpgradeCompleteMessage());
    self::assertEquals('You stopped the upgrade. Your plan has not been changed.', $planChangeConfig->getUpgradeCanceledMessage());

    self::assertEquals('Downgrade', $planChangeConfig->getDowngradeButtonLabel());
    self::assertEquals('You are about to downgrade from [from-variation:label] to [to-variation:label]. The changes will take effect at the start of the next billing period ([next-billing-period:start]).', $planChangeConfig->getDowngradeConfirmText());
    self::assertEquals('Schedule downgrade', $planChangeConfig->getDowngradeConfirmButtonLabel());
    self::assertEquals('Do not downgrade', $planChangeConfig->getDowngradeCancelButtonLabel());
    self::assertEquals('Your plan is scheduled to be downgraded on [next-billing-period:start].', $planChangeConfig->getDowngradeCompleteMessage());
    self::assertEquals('You stopped the downgrade. Your plan has not been changed.', $planChangeConfig->getDowngradeCanceledMessage());

    self::assertEquals('Cancel', $planChangeConfig->getCancelButtonLabel());
    self::assertEquals('You are about to cancel your [from-variation:label] subscription. Your subscription will expire on [billing-period:end].', $planChangeConfig->getCancelConfirmText());
    self::assertEquals('Schedule cancellation', $planChangeConfig->getCancelConfirmButtonLabel());
    self::assertEquals('Do not cancel', $planChangeConfig->getCancelCancelButtonLabel());
    self::assertEquals("You've canceled your subscription and you will lose access on [billing-period:end].", $planChangeConfig->getCancelCompleteMessage());
    self::assertEquals('You stopped the cancellation. You are still subscribed.', $planChangeConfig->getCancelCanceledMessage());

    // Modify a field and save the form.
    $this->submitForm([
      'label' => 'Modified test PCUI config',
    ], 'Save');

    // Reload the entity.
    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfigInterface $planChangeConfig */
    $planChangeConfig = $this->container->get('entity_type.manager')
      ->getStorage('commerce_recurring_pcui_config')
      ->load('test_pcui_config');

    // Verify that it has been saved properly.
    self::assertEquals('Modified test PCUI config', $planChangeConfig->label());
  }

  /**
   * Test deleting a plan change configuration.
   */
  public function testFormDelete() {
    $this->markTestIncomplete();
  }

}
