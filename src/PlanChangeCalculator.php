<?php

namespace Drupal\commerce_recurring_pcui;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * PlanChangeCalculator service.
 */
class PlanChangeCalculator implements PlanChangeCalculatorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PlanChangeCalculator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangeType(SubscriptionInterface $subscription, ProductVariationInterface $new_variation): string {
    if ($subscription->hasPurchasedEntity()) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $original_variation */
      $original_variation = $subscription->getPurchasedEntity();

      // Todo take promotions into account.
      $original_price = $original_variation->getPrice();
      $new_price = $new_variation->getPrice();

      if ($new_price->greaterThan($original_price)) {
        return self::PCUI_DIRECTION_UPGRADE;
      }
      // todo: what if they're equal.
      else {
        return self::PCUI_DIRECTION_DOWNGRADE;
      }
    }
  }

}
