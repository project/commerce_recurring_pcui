<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

/**
 * Test upgrades, downgrades, and cancellations.
 */
class PlanChangeTest extends PcuiTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_recurring_pcui',
    'commerce_checkout',
    'commerce_payment',
    'commerce_payment_example',
    'commerce_order',
  ];

  /**
   * Test payment information to submit during checkout.
   *
   * @var string[]
   */
  protected $payment = [
    'payment_information[add_payment_method][payment_details][number]' => '4111111111111111',
    'payment_information[add_payment_method][payment_details][expiration][month]' => '12',
    'payment_information[add_payment_method][payment_details][expiration][year]' => '2029',
    'payment_information[add_payment_method][payment_details][security_code]' => '123',
    'payment_information[add_payment_method][billing_information][address][0][address][given_name]' => 'GivenName',
    'payment_information[add_payment_method][billing_information][address][0][address][family_name]' => 'FamilyName',
    'payment_information[add_payment_method][billing_information][address][0][address][address_line1]' => '123 Test St',
    'payment_information[add_payment_method][billing_information][address][0][address][locality]' => 'New York',
    'payment_information[add_payment_method][billing_information][address][0][address][administrative_area]' => 'NY',
    'payment_information[add_payment_method][billing_information][address][0][address][postal_code]' => '10001',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_payment_gateway',
      'administer commerce_payment',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp() {
    parent::setUp();

    // Add the PCUI trait to the product type.
    $this->addTraitToDefaultProductType();

    // Create a PCUI config.
    $this->createPlanChangeConfig();

    // Create a billing schedule.
    $this->createBillingSchedule();

    // Create multiple product variations (need lower price and higher price
    // options to test upgrades and downgrades).
    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => 'initial',
      'price' => [
        'number' => 19.99,
        'currency_code' => 'USD',
      ],
      'subscription_type' => 'product_variation',
      'billing_schedule' => 'test_pcui_billing_schedule',
    ]);

    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => 'downgrade',
      'price' => [
        'number' => 9.99,
        'currency_code' => 'USD',
      ],
      'subscription_type' => 'product_variation',
      'billing_schedule' => 'test_pcui_billing_schedule',
    ]);

    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => 'upgrade',
      'price' => [
        'number' => 29.99,
        'currency_code' => 'USD',
      ],
      'subscription_type' => 'product_variation',
      'billing_schedule' => 'test_pcui_billing_schedule',
    ]);

    // Create a product.
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'My product',
      'variations' => $variations,
      'stores' => [$this->store],
      'pcui_config' => 'test_pcui_config',
    ]);

    // Create a payment gateway.
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->createEntity('commerce_payment_gateway', [
      'id' => 'onsite',
      'label' => 'On-site',
      'plugin' => 'example_onsite',
      'configuration' => [
        'api_key' => '2342fewfsfs',
        'payment_method_types' => ['credit_card'],
      ],
    ]);
    $payment_gateway->save();

    // Login a default user.
    $this->drupalLogin($this->loggedInUser = $this->drupalCreateUser());

    // Purchase the initial subscription.
    $this->drupalGet('/product/1');
    $this->assertSession()->buttonExists('Add to cart');
    $this->submitForm([], 'Add to cart');
    $this->drupalGet('/checkout/1/order_information');
    $this->assertText('My product');
    $this->assertText('$19.99');
    $this->assertSession()->buttonExists('Continue to review');
    $this->submitForm($this->payment, 'Continue to review');
    $this->assertSession()->buttonExists('Pay and complete purchase');
    $this->submitForm([], 'Pay and complete purchase');
  }

  /**
   * Test upgrades.
   */
  public function testUpgrade() {
    self::markTestIncomplete();

  }

  /**
   * Test downgrades.
   */
  public function testDowngrade() {
    self::markTestIncomplete();

  }

  /**
   * Test cancellation.
   */
  public function testCancel() {
    self::markTestIncomplete();
  }

}
