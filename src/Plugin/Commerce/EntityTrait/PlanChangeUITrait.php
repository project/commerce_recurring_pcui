<?php

namespace Drupal\commerce_recurring_pcui\Plugin\Commerce\EntityTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;

/**
 * Provides a trait to enable purchasing of subscriptions.
 *
 * @CommerceEntityTrait(
 *   id = "pcui_plan_change",
 *   label = @Translation("Allow configuring plan change form (upgrades, downgrades, cancellations)"),
 *   entity_types = {"commerce_product"}
 * )
 */
class PlanChangeUITrait extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['pcui_config'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Plan change configuration'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_recurring_pcui_config')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
