<?php

namespace Drupal\Tests\commerce_recurring_pcui\Kernel;

use Drupal\commerce_recurring_pcui\PlanChangeCalculator;

/**
 * @coversDefaultClass \Drupal\commerce_recurring_pcui\PlanChangeCalculator
 * @group commerce_recurring_pcui
 */
class PlanChangeCalculatorTest extends PcuiKernelTestBase {

  /**
   * @covers ::getChangeType
   */
  public function testCalculator() {
    $calculator = new PlanChangeCalculator($this->entityTypeManager);
    $this->assertEquals('commerce_recurring_pcui.direction.upgrade', $calculator->getChangeType($this->subscription, $this->variationUpgrade));
    $this->assertEquals('commerce_recurring_pcui.direction.downgrade', $calculator->getChangeType($this->subscription, $this->variationDowngrade));
  }

}
