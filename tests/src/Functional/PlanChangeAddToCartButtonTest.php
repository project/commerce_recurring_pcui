<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

/**
 * Test that our special add-to-cart button widget works.
 */
class PlanChangeAddToCartButtonTest extends PcuiTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // TODO: configure commerce_variation_cart_form settings. then the first assertion should pass.
    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 9.99,
        'currency_code' => 'USD',
      ],
    ]);

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 19.99,
        'currency_code' => 'USD',
      ],
    ]);

    $variations[] = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 29.99,
        'currency_code' => 'USD',
      ],
    ]);

    $this->product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'My product',
      'variations' => $variations,
      'stores' => [$this->store],
    ]);

    $this->drupalLogin($this->loggedInUser = $this->drupalCreateUser());
  }

  /**
   * Test that the Upgrade button appears.
   */
  public function testUpgradeButton() {
    $this->drupalGet($this->product->toUrl());

    self::markTestIncomplete();

    // If the user is not subscribed, this should be a view of variations.
    $this->assertSession()->pageTextContains('Add to cart');
  }

  /**
   * Test that the downgrade button appears.
   */
  public function testDowngradeButton() {
    self::markTestIncomplete();
    // TODO: assert correct Downgrade button displayed.
  }

  /**
   * Test that the cancel button appears.
   */
  public function testCancelButton() {
    self::markTestIncomplete();
    // TODO: assert correct Cancel button displayed.
  }

}
