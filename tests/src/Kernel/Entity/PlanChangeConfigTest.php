<?php

namespace Drupal\Tests\commerce_recurring_pcui\Kernel\Entity;

use Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig
 */
class PlanChangeConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce',
    'commerce_recurring',
    'commerce_recurring_pcui',
  ];

  /**
   * @covers ::id
   * @covers ::label
   * @covers ::getStrategyPluginId
   * @covers ::setStrategyPluginId
   * @covers ::getUpgradeButtonLabel
   * @covers ::getUpgradeConfirmText
   * @covers ::getUpgradeConfirmButtonLabel
   * @covers ::getUpgradeCancelButtonLabel
   * @covers ::getUpgradeCompleteMessage
   * @covers ::getUpgradeCanceledMessage
   * @covers ::getDowngradeButtonLabel
   * @covers ::getDowngradeConfirmText
   * @covers ::getDowngradeConfirmButtonLabel
   * @covers ::getDowngradeCancelButtonLabel
   * @covers ::getDowngradeCompleteMessage
   * @covers ::getDowngradeCanceledMessage
   * @covers ::getCancelButtonLabel
   * @covers ::getCancelConfirmText
   * @covers ::getCancelConfirmButtonLabel
   * @covers ::getCancelCancelButtonLabel
   * @covers ::getCancelCompleteMessage
   * @covers ::getCancelCanceledMessage
   */
  public function testPlanChangeConfig() {
    PlanChangeConfig::create([
      'label' => 'Test PCUI config',
      'id' => 'test_pcui_config',
      'strategy' => 'allow_prorating',

      'upgradeButtonLabel' => 'upgradeButtonLabel',
      'upgradeConfirmText' => 'upgradeConfirmText',
      'upgradeConfirmButtonLabel' => 'upgradeConfirmButtonLabel',
      'upgradeCancelButtonLabel' => 'upgradeCancelButtonLabel',
      'upgradeCompleteMessage' => 'upgradeCompleteMessage',
      'upgradeCanceledMessage' => 'upgradeCanceledMessage',

      'downgradeButtonLabel' => 'downgradeButtonLabel',
      'downgradeConfirmText' => 'downgradeConfirmText',
      'downgradeConfirmButtonLabel' => 'downgradeConfirmButtonLabel',
      'downgradeCancelButtonLabel' => 'downgradeCancelButtonLabel',
      'downgradeCompleteMessage' => 'downgradeCompleteMessage',
      'downgradeCanceledMessage' => 'downgradeCanceledMessage',

      'cancelButtonLabel' => 'cancelButtonLabel',
      'cancelConfirmText' => 'cancelConfirmText',
      'cancelConfirmButtonLabel' => 'cancelConfirmButtonLabel',
      'cancelCancelButtonLabel' => 'cancelCancelButtonLabel',
      'cancelCompleteMessage' => 'cancelCompleteMessage',
      'cancelCanceledMessage' => 'cancelCanceledMessage',
    ])->save();

    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig $plan_change_config */
    $plan_change_config = PlanChangeConfig::load('test_pcui_config');

    $this->assertEquals('test_pcui_config', $plan_change_config->id());
    $this->assertEquals('Test PCUI config', $plan_change_config->label());

    $this->assertEquals('allow_prorating', $plan_change_config->getStrategyPluginId());
    $plan_change_config->setStrategyPluginId('avoid_prorating');
    $this->assertEquals('avoid_prorating', $plan_change_config->getStrategyPluginId());

    // Integrity checks.
    $this->assertEquals('upgradeButtonLabel', $plan_change_config->getUpgradeButtonLabel());
    $this->assertEquals('upgradeConfirmText', $plan_change_config->getUpgradeConfirmText());
    $this->assertEquals('upgradeConfirmButtonLabel', $plan_change_config->getUpgradeConfirmButtonLabel());
    $this->assertEquals('upgradeCancelButtonLabel', $plan_change_config->getUpgradeCancelButtonLabel());
    $this->assertEquals('upgradeCompleteMessage', $plan_change_config->getUpgradeCompleteMessage());
    $this->assertEquals('upgradeCanceledMessage', $plan_change_config->getUpgradeCanceledMessage());

    $this->assertEquals('downgradeButtonLabel', $plan_change_config->getDowngradeButtonLabel());
    $this->assertEquals('downgradeConfirmText', $plan_change_config->getDowngradeConfirmText());
    $this->assertEquals('downgradeConfirmButtonLabel', $plan_change_config->getDowngradeConfirmButtonLabel());
    $this->assertEquals('downgradeCancelButtonLabel', $plan_change_config->getDowngradeCancelButtonLabel());
    $this->assertEquals('downgradeCompleteMessage', $plan_change_config->getDowngradeCompleteMessage());
    $this->assertEquals('downgradeCanceledMessage', $plan_change_config->getDowngradeCanceledMessage());

    $this->assertEquals('cancelButtonLabel', $plan_change_config->getCancelButtonLabel());
    $this->assertEquals('cancelConfirmText', $plan_change_config->getCancelConfirmText());
    $this->assertEquals('cancelConfirmButtonLabel', $plan_change_config->getCancelConfirmButtonLabel());
    $this->assertEquals('cancelCancelButtonLabel', $plan_change_config->getCancelCancelButtonLabel());
    $this->assertEquals('cancelCompleteMessage', $plan_change_config->getCancelCompleteMessage());
    $this->assertEquals('cancelCanceledMessage', $plan_change_config->getCancelCanceledMessage());
  }

}
