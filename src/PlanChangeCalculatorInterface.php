<?php

namespace Drupal\commerce_recurring_pcui;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;

/**
 * Calculates billing period and price changes when switching plans.
 */
interface PlanChangeCalculatorInterface {

  /**
   * Indicates a change to a higher-tier plan.
   */
  public const PCUI_DIRECTION_UPGRADE = 'commerce_recurring_pcui.direction.upgrade';

  /**
   * Indicates a change to a lower-tier plan.
   */
  public const PCUI_DIRECTION_DOWNGRADE = 'commerce_recurring_pcui.direction.downgrade';

  /**
   * Indicates that the plan is being canceled.
   */
  public const PCUI_DIRECTION_CANCEL = 'commerce_recurring_pcui.direction.cancel';

  /**
   * Determine if a plan change would be an upgrade, downgrade, or cancellation.
   *
   * @param \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription
   *   The customer's current subscription.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $new_variation
   *   The product variation the customer intends to change to. This MUST be
   *   part of the same product as their existing plan.
   *
   * @return string
   *   Returns the appropriate interface constant based on whether this is an
   *   upgrade, downgrade, or cancellation.
   */
  public function getChangeType(SubscriptionInterface $subscription, ProductVariationInterface $new_variation): string;

}
