<?php

namespace Drupal\commerce_recurring_pcui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Plan Change Configuration entities.
 */
interface PlanChangeConfigInterface extends ConfigEntityInterface {

  /**
   * Gets the plan change strategy.
   */
  public function getStrategyPluginId(): string;

  /**
   * Sets the plan change strategy.
   *
   * @param string $strategy_plugin_id
   *   The plugin ID of the plan change strategy.
   *
   * @todo: fix arguments. should require an actual instance
   */
  public function setStrategyPluginId(string $strategy_plugin_id): void;

  /**
   * Gets the label for the button that starts the upgrade.
   */
  public function getUpgradeButtonLabel(): string;

  /**
   * Gets the text shown on the upgrade confirmation page.
   */
  public function getUpgradeConfirmText(): string;

  /**
   * Gets the label for the button that confirms the upgrade.
   */
  public function getUpgradeConfirmButtonLabel(): string;

  /**
   * Gets the label for the do-not-upgrade button.
   */
  public function getUpgradeCancelButtonLabel(): string;

  /**
   * Gets the message shown upon successful upgrade.
   */
  public function getUpgradeCompleteMessage(): string;

  /**
   * Gets the message shown after an upgrade is canceled.
   */
  public function getUpgradeCanceledMessage(): string;

  /**
   * Gets the label for the button that starts the upgrade.
   */
  public function getDowngradeButtonLabel(): string;

  /**
   * Gets the text shown on the upgrade confirmation page.
   */
  public function getDowngradeConfirmText(): string;

  /**
   * Gets the label for the button that confirms the upgrade.
   */
  public function getDowngradeConfirmButtonLabel(): string;

  /**
   * Gets the label for the do-not-upgrade button.
   */
  public function getDowngradeCancelButtonLabel(): string;

  /**
   * Gets the message shown upon successful upgrade.
   */
  public function getDowngradeCompleteMessage(): string;

  /**
   * Gets the message shown after an upgrade is canceled.
   */
  public function getDowngradeCanceledMessage(): string;

  /**
   * Gets the label for the button that starts cancellation.
   */
  public function getCancelButtonLabel(): string;

  /**
   * Gets the text shown on the cancellation confirmation page.
   */
  public function getCancelConfirmText(): string;

  /**
   * Gets the label for the button that confirms cancellation.
   */
  public function getCancelConfirmButtonLabel(): string;

  /**
   * Gets the label for the do-not-cancel button.
   */
  public function getCancelCancelButtonLabel(): string;

  /**
   * Gets the message shown upon successful cancellation.
   */
  public function getCancelCompleteMessage(): string;

  /**
   * Gets the message shown after canceling is canceled.
   */
  public function getCancelCanceledMessage(): string;

}
