<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

/**
 * Test description.
 *
 * @group commerce_recurring_pcui
 */
class TraitTest extends PcuiTestBase {

  /**
   * Test callback.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function testThatEntityTraitAddsProductFields() {
    $this->addTraitToDefaultProductType();

    $this->drupalGet('/admin/commerce/config/product-types/default/edit');
    $this->assertSession()->checkboxChecked('traits[pcui_plan_change]');

    // The field was created.
    $this->drupalGet('admin/commerce/config/product-types/default/edit/fields');
    $this->assertSession()->pageTextContains('Plan change configuration');
  }

}
