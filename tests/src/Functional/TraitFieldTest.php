<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

use Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig;

/**
 * Tests the trait field when enabled on a product type.
 */
class TraitFieldTest extends PcuiTestBase {

  /**
   * Test that we can set up and access a PCUI config properly.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function testPcuiConfigFields() {
    $this->addTraitToDefaultProductType();

    $this->createPlanChangeConfig();

    $this->drupalGet('/product/add/default');

    $this->assertSession()->pageTextContains('Plan change configuration');
    $this->assertSession()->optionExists('pcui_config', 'test_pcui_config');

    $this->submitForm([
      'title[0][value]' => 'Test product with PCUI',
      'pcui_config' => 'test_pcui_config',
      'stores[target_id][value][1]' => 1,
    ], 'Save');

    $entityTypeManager = $this->container->get('entity_type.manager');
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = $entityTypeManager
      ->getStorage('commerce_product')
      ->load(1);
    /** @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig $pcuiConfig */
    $pcuiConfig = $product->get('pcui_config')
      ->first()
      ->get('entity')
      ->getTarget()
      ->getValue();
    self::assertInstanceOf(PlanChangeConfig::class, $pcuiConfig, 'pcui_config points to a PlanChangeConfig object.');
    self::assertEquals('test_pcui_config', $pcuiConfig->id(), 'pcui_config points to the correct Plan Change Configuration.');
  }

}
