<?php

namespace Drupal\Tests\commerce_recurring_pcui\Kernel;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring\Entity\Subscription;
use Drupal\commerce_recurring_pcui\Entity\PlanChangeConfig;
use Drupal\Tests\commerce_recurring\Kernel\RecurringKernelTestBase;
use Drupal\commerce_recurring\Entity\BillingSchedule;

/**
 * Class PcuiKernelTestBase.
 *
 * @package Drupal\Tests\commerce_recurring_pcui\Kernel
 */
abstract class PcuiKernelTestBase extends RecurringKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_recurring_pcui',
  ];

  /**
   * The test product that has a plan change config trait.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * The test variation for a downgrade.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variationDowngrade;

  /**
   * The test variation for an upgrade.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variationUpgrade;

  /**
   * The plan change configuration.
   *
   * @var \Drupal\commerce_recurring_pcui\Entity\PlanChangeConfigInterface
   */
  protected $planChangeConfig;

  /**
   * The subscription.
   *
   * @var \Drupal\commerce_recurring\Entity\SubscriptionInterface
   */
  protected $subscription;

  /**
   * Sets up a user with a subscription to test several cases.
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['commerce_recurring_pcui']);
    $this->installEntitySchema('commerce_recurring_pcui_config');

    // Create a PlanChangeConfig.
    $plan_change_config = PlanChangeConfig::create([
      'label' => 'Test PCUI config',
      'id' => 'test_pcui_config',
      'strategy' => 'allow_prorating',

      'upgradeButtonLabel' => 'Upgrade',
      'upgradeConfirmText' => 'You are about to upgrade from [from-variation:label] to [to-variation:label]. You will be charged [upgrade-order:order-price] immediately. Would you like to finish the upgrade?',
      'upgradeConfirmButtonLabel' => 'Finish upgrade',
      'upgradeCancelButtonLabel' => 'Do not upgrade',
      'upgradeCompleteMessage' => 'Your plan has been changed.',
      'upgradeCanceledMessage' => 'You stopped the upgrade. Your plan has not been changed.',

      'downgradeButtonLabel' => 'Downgrade',
      'downgradeConfirmText' => 'You are about to downgrade from [from-variation:label] to [to-variation:label]. The changes will take effect at the start of the next billing period ([next-billing-period:start]).',
      'downgradeConfirmButtonLabel' => 'Schedule downgrade',
      'downgradeCancelButtonLabel' => 'Do not downgrade',
      'downgradeCompleteMessage' => 'Your plan is scheduled to be downgraded on [next-billing-period:start].',
      'downgradeCanceledMessage' => 'You stopped the downgrade. Your plan has not been changed.',

      'cancelButtonLabel' => 'Cancel',
      'cancelConfirmText' => 'You are about to cancel your [from-variation:label] subscription. Your subscription will expire on [billing-period:end].',
      'cancelConfirmButtonLabel' => 'Schedule cancellation',
      'cancelCancelButtonLabel' => 'Do not cancel',
      'cancelCompleteMessage' => "You've canceled your subscription and you will lose access on [billing-period:end].",
      'cancelCanceledMessage' => 'You stopped the cancellation. You are still subscribed.',
    ]);
    $plan_change_config->save();
    $this->planChangeConfig = $this->reloadEntity($plan_change_config);

    // Attach the trait to the Product type.
    $trait_manager = \Drupal::service('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('pcui_plan_change');
    $trait_manager->installTrait($trait, 'commerce_product', 'default');

    // Modify the billing schedule created in the parent class.
    $configuration = $this->billingSchedule->getPluginConfiguration();
    unset($configuration['trial_interval']);
    $this->billingSchedule->setPluginConfiguration($configuration);
    $this->billingSchedule->setBillingType(BillingSchedule::BILLING_TYPE_PREPAID);
    $this->billingSchedule->save();

    // Create downgrade and upgrade variations.
    $variation_downgrade = ProductVariation::create([
      'type' => 'default',
      'title' => 'downgrade',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '1.00',
        'currency_code' => 'USD',
      ],
      'subscription_type' => [
        'target_plugin_id' => 'product_variation',
      ],
      'billing_schedule' => $this->billingSchedule,
    ]);
    $variation_downgrade->save();
    $this->variationDowngrade = $this->reloadEntity($variation_downgrade);

    $variation_upgrade = ProductVariation::create([
      'type' => 'default',
      'title' => 'upgrade',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '20.00',
        'currency_code' => 'USD',
      ],
      'subscription_type' => [
        'target_plugin_id' => 'product_variation',
      ],
      'billing_schedule' => $this->billingSchedule,
    ]);
    $variation_upgrade->save();
    $this->variationUpgrade = $this->reloadEntity($variation_upgrade);

    // Create a product with the Plan Change Config trait attached.
    $product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'variations' => [
        $this->variation,
        $this->variationDowngrade,
        $this->variationUpgrade,
      ],
      'stores' => [$this->store],
      'pcui_config' => 'test_pcui_config',
    ]);
    $product->save();
    $this->product = $this->reloadEntity($product);

    // @todo: determine if we need order item, order.
    //    $order_item = OrderItem::create([
    //      'type' => 'default',
    //      'title' => $this->variation->getOrderItemTitle(),
    //      'purchased_entity' => $this->variation->id(),
    //      'unit_price' => $this->variation->getPrice(),
    //    ]);
    //    $order_item->save();
    //    $order = Order::create([
    //      'type' => 'default',
    //      'store_id' => $this->store->id(),
    //      'uid' => $this->user->id(),
    //      'order_items' => [$order_item],
    //      'state' => 'completed',
    //    ]);
    //    $order->save();
    // Create a subscription.
    $subscription = Subscription::create([
      'type' => 'product_variation',
      'store_id' => $this->store->id(),
      'uid' => $this->user->id(),
      'billing_schedule' => $this->billingSchedule,
      'purchased_entity' => $this->variation,
      'title' => 'Test subscription',
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
      'state' => 'active',
      'starts' => strtotime('2020-08-25 13:00:00'),
    // 'ends' => strtotime('2020-08-25 14:00:00'),
    ]);
    $subscription->save();
    $this->subscription = $this->reloadEntity($subscription);
  }

}
