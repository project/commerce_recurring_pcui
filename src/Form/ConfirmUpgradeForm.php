<?php

namespace Drupal\commerce_recurring_pcui\Form;

/**
 * Provides a confirmation form for plan upgrades.
 */
class ConfirmUpgradeForm extends PlanChangeFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_recurring_pcui_confirm_upgrade';
  }

}
