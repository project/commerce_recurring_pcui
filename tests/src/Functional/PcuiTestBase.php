<?php

namespace Drupal\Tests\commerce_recurring_pcui\Functional;

use Drupal\commerce_recurring\Entity\BillingScheduleInterface;
use Drupal\Tests\commerce_product\Functional\ProductBrowserTestBase;

/**
 * Common base class for Commerce Recurring PCUI tests.
 *
 * Functionality only needed by some tests can be provided via traits.
 */
class PcuiTestBase extends ProductBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['commerce_recurring_pcui'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_recurring_pcui_config',
      'administer commerce_billing_schedule',
      'administer commerce_subscription',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Adds the pcui_plan_change trait to the default product type.
   */
  protected function addTraitToDefaultProductType(): void {
    $this->drupalGet('/admin/commerce/config/product-types/default/edit');
    $this->submitForm([
      'traits[pcui_plan_change]' => 'pcui_plan_change',
    ], t('Save'));

    // Clear the field cache.
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();
  }

  /**
   * Creates a plan change configuration using the UI.
   *
   * @param string $id
   *   OPTIONAL allows us create multiple plans in a single test.
   */
  protected function createPlanChangeConfig(string $id = 'test_pcui_config') {
    $this->drupalGet('/admin/commerce/config/plan-change-config/add');
    $this->submitForm([
      'label' => 'Test PCUI config',
      'id' => $id,
      'strategy' => 'allow_prorating',

      'upgradeButtonLabel' => 'Upgrade',
      'upgradeConfirmText' => 'You are about to upgrade from [from-variation:label] to [to-variation:label]. You will be charged [upgrade-order:order-price] immediately. Would you like to finish the upgrade?',
      'upgradeConfirmButtonLabel' => 'Finish upgrade',
      'upgradeCancelButtonLabel' => 'Do not upgrade',
      'upgradeCompleteMessage' => 'Your plan has been changed.',
      'upgradeCanceledMessage' => 'You stopped the upgrade. Your plan has not been changed.',

      'downgradeButtonLabel' => 'Downgrade',
      'downgradeConfirmText' => 'You are about to downgrade from [from-variation:label] to [to-variation:label]. The changes will take effect at the start of the next billing period ([next-billing-period:start]).',
      'downgradeConfirmButtonLabel' => 'Schedule downgrade',
      'downgradeCancelButtonLabel' => 'Do not downgrade',
      'downgradeCompleteMessage' => 'Your plan is scheduled to be downgraded on [next-billing-period:start].',
      'downgradeCanceledMessage' => 'You stopped the downgrade. Your plan has not been changed.',

      'cancelButtonLabel' => 'Cancel',
      'cancelConfirmText' => 'You are about to cancel your [from-variation:label] subscription. Your subscription will expire on [billing-period:end].',
      'cancelConfirmButtonLabel' => 'Schedule cancellation',
      'cancelCancelButtonLabel' => 'Do not cancel',
      'cancelCompleteMessage' => "You've canceled your subscription and you will lose access on [billing-period:end].",
      'cancelCanceledMessage' => 'You stopped the cancellation. You are still subscribed.',
    ], 'Save');

    $this->assertSession()->pageTextContains('Created the Test PCUI config plan change configuration.');
  }

  /**
   * Creates a Commerce Recurring billing schedule.
   */
  protected function createBillingSchedule() : void {
    $this->drupalGet('/admin/commerce/config/billing-schedules/add');
    $this->submitForm([
      'label' => 'PCUI test',
      'id' => 'test_pcui_billing_schedule',
      'displayLabel' => 'Billing Schedule for PCUI test',
      // Copied from
      // Drupal\commerce_recurring\FunctionalJavascript\BillingScheduleTest.
      'billingType' => BillingScheduleInterface::BILLING_TYPE_POSTPAID,
      'dunning[retry][0]' => '1',
      'dunning[retry][1]' => '2',
      'dunning[retry][2]' => '3',
      'dunning[unpaid_subscription_state]' => 'canceled',
      'plugin' => 'fixed',
      'configuration[fixed][trial_interval][allow_trials]' => 1,
      'configuration[fixed][trial_interval][number]' => '2',
      'configuration[fixed][trial_interval][unit]' => 'month',
      'configuration[fixed][interval][number]' => '2',
      'configuration[fixed][interval][unit]' => 'month',
      'configuration[fixed][start_day]' => '4',
      'prorater' => 'proportional',
    ], 'Save');

    $this->assertSession()->pageTextContains('Saved the PCUI test billing schedule.');
  }

}
